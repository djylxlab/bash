#!/bin/bash

echo "Veuillez saisir un nombre entre 1 et 1000 afin de trouver le numéro secret :"
read nombre


nombreMystere=$(( $RANDOM % 1000))

while [ "$nombre" != "$nombreMystere" ]
do
	if [ -z "$nombre" ] || ! [[ "$nombre" =~ ^[0-9]+$ ]]
	then
		echo "Non valide, vous devez saisir un nombre !!"
		echo "Veullez saisir un nombre :"
		read nombre
	elif [ "$nombre" -lt "$nombreMystere" ]
	then
		echo "Votre nombre est plus petit que le nombre Mystère !"
		echo "Veuillez saisir un nombre :"
		read nombre
	elif [ "$nombre" -gt "$nombreMystere" ]
	then
		echo "VOtre nombre est plus grand que le nombre Mystere !"
		echo "Veuillez saisir un nombre :"
		read nombre
	fi
done
if [ "$nombre" -eq "$nombreMystere" ]
then
	echo "Bravo, vous avez trouvé le nombre mystère ($nombreMystere) !"
fi
